"use strict";
const roads = [
    "Alice's House-Bob's House",
    "Alice's House-Post Office",
    "Alice's House-Cabin",
    "Daria's House-Emie's House",
    "Daria's House-Town Hall",
    "Emie's House-Grete's House",
    "Grete's House-Shop",
    "Grete's House-Farm",
    "Marketplace-Post Office",
    "Marketplace-Town Hall",
    "Marketplace-Farm",
    "Marketplace-Shop",
    "Shop-Town Hall",
];
function buildGraph(roads) {
    let graph = Object.create(null);
    function addEdge(from, to) {
        if (graph[from] == null) {
            graph[from] = [to];
        }
        else {
            graph[from].push(to);
        }
    }
    for (let [from, to] of roads.map(_ => _.split('-'))) {
        addEdge(from, to);
        addEdge(to, from);
    }
    return Object.freeze(graph);
}
const roadGraph = buildGraph(roads);
class VillageState {
    constructor(place, parcels) {
        this.place = place;
        this.parcels = parcels;
    }
    move(destination) {
        if (!roadGraph[this.place].includes(destination)) {
            return this;
        }
        else {
            let parcels = this.parcels.map(parcel => {
                if (parcel.place != this.place)
                    return parcel;
                return {
                    place: destination,
                    address: parcel.address,
                };
            }).filter(parcel => parcel.place != parcel.address);
            return new VillageState(destination, parcels);
        }
    }
    static random(parcelCount = 5) {
        let parcels = [];
        for (let i = 0; i < parcelCount; i++) {
            let address = randomPick(Object.keys(roadGraph));
            let place;
            do {
                place = randomPick(Object.keys(roadGraph));
            } while (place === address);
            parcels.push({ place, address });
        }
        console.log({ parcels });
        return new VillageState("Post Office", parcels);
    }
}
function runRobot(state, robot, memory) {
    for (let turn = 0;; turn++) {
        if (state.parcels.length == 0) {
            console.log(`Done in ${turn} turns`);
            break;
        }
        let action = robot(state, memory);
        state = state.move(action.direction);
        memory = action.memory;
        console.log(`Moved to ${action.direction}`);
    }
}
function randomPick(array) {
    let choice = Math.floor(Math.random() * array.length);
    return array[choice];
}
function randomRobot(state) {
    return {
        direction: randomPick(roadGraph[state.place]),
    };
}
const mailRoute = [
    "Alice's House",
    "Cabin",
    "Alice's House",
    "Bob's House",
    "Town Hall",
    "Daria's House",
    "Emie's House",
    "Grete's House",
    "Shop",
    "Grete's House",
    "Farm",
    "Marketplace",
    "Post Office",
];
function routeRobot(state, memory = []) {
    if (memory.length == 0)
        memory = mailRoute;
    return {
        direction: memory[0],
        memory: memory.slice(1)
    };
}
function findRoute(graph, from, to) {
    let work = [
        { at: from, route: [] },
    ];
    for (let i = 0; i < work.length; i++) {
        let { at, route } = work[i];
        for (let place of graph[at]) {
            if (place == to)
                return route.concat(place);
            if (!work.some(w => w.at == place)) {
                work.push({ at: place, route: route.concat(place) });
            }
        }
    }
    return [];
}
function goalOrientedRobot(state, memory = []) {
    if (memory.length == 0) {
        let parcel = state.parcels[0];
        if (parcel.place != state.place) {
            memory = findRoute(roadGraph, state.place, parcel.place);
        }
        else {
            memory = findRoute(roadGraph, state.place, parcel.address);
        }
    }
    return {
        direction: memory[0],
        memory: memory.slice(1)
    };
}
runRobot(VillageState.random(5), goalOrientedRobot);
//# sourceMappingURL=07.js.map