"use strict";
function pureConcat(original, items) {
    return [...original, ...items];
}
function flat(items) {
    return items.reduce((flatten, item) => pureConcat(flatten, item), []);
}
/**
console.log(
    flat([[1, 2, 3], [2, 4, 5], [2]])
)
**/
function loop(start, testFunc, updateFunc, aBody) {
    for (let i = start; testFunc(i); i = updateFunc(i))
        aBody(i);
}
/**
loop(1, i => i < 10, i => i + 1, () => console.log('Opa'));
**/
function everyFor(items, aTest) {
    for (let item of items) {
        if (!aTest(item))
            return false;
    }
    return true;
}
function everySome(items, aTest) {
    return !items.some(item => !aTest(item));
}
/**
const aTestFunc = (item: number) => item < 10

const listNumberSuc: number[] = [1, 4, 6, 8, 9];
const listNumberFai: number[] = [1, 4, 6, 8, 12];

console.log(
    everyFor(listNumberSuc, aTestFunc),
    everySome(listNumberSuc, aTestFunc),
)

console.log(
    everyFor(listNumberFai, aTestFunc),
    everySome(listNumberFai, aTestFunc),
)
**/
//# sourceMappingURL=05.js.map