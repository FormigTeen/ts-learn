"use strict";
function range(start, end) {
    const totalElements = (end - start) + 1;
    return [...Array(totalElements).keys()].map(index => index + start);
}
function sum(args) {
    return args.reduce((total, number) => total + number, 0);
}
function reverse(elements) {
    const result = [];
    for (let i = elements.length - 1; i >= 0; i--) {
        const index = elements.length - i - 1;
        result[index] = elements[i];
    }
    return result;
}
function reverseInPlace(elements) {
    const copy = [...elements];
    for (let i = elements.length - 1; i >= 0; i--) {
        const index = elements.length - i - 1;
        elements[index] = copy[i];
    }
    return elements;
}
console.log(reverse([1, 2, 3]));
console.log(reverseInPlace([1, 2, 3]));
console.log(sum(range(1, 100)));
//# sourceMappingURL=04.js.map