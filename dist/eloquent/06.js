"use strict";
class Vec {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    get length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }
    plus(vecA) {
        return new Vec(this.x + vecA.x, this.y + vecA.y);
    }
    minus(vecA) {
        return new Vec(this.x - vecA.x, this.y - vecA.y);
    }
}
class Group {
    constructor(items = []) {
        this.items = items;
    }
    has(item) {
        return this.items.indexOf(item) >= 0;
    }
    add(item) {
        if (this.has(item))
            return false;
        this.items.push(item);
        return true;
    }
    delete(item) {
        if (!this.has(item))
            return false;
        this.items.splice(this.items.indexOf(item), 1);
        return true;
    }
    get(index) {
        return this.items[index];
    }
    get length() {
        return this.items.length;
    }
}
class GroupIterator {
    constructor(group) {
        this.group = group;
        this.index = 0;
    }
    next() {
        if (this.index === this.group.length) {
            return {
                done: true
            };
        }
        const value = this.group.get(this.index);
        this.index = this.index + 1;
        return {
            done: false,
            value
        };
    }
}
//# sourceMappingURL=06.js.map