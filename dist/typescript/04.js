"use strict";
const mapTreeArrow = (node, closure) => (Object.assign(Object.assign({}, node), { value: closure(node.value) }));
function mapTree(node, closure) {
    return Object.assign(Object.assign({}, node), { value: closure(node.value) });
}
const a = { value: 'a' };
const b = { value: 'b', isLeaf: true };
const c = { value: 'c', children: [b] };
const a1 = mapTree(a, _ => _.toUpperCase());
const b1 = mapTree(b, _ => _.toUpperCase());
const c1 = mapTree(c, _ => _.toUpperCase());
function logPerimeter(s) {
    console.log(s.numberOfSides * s.sideLength);
    return s;
}
const square = {
    numberOfSides: 4,
    sideLength: 3
};
logPerimeter(square);
function call(f, ...args) {
    return f(...args);
}
function fill(length, value) {
    return Array.from({ length }, () => value);
}
function is(...args) {
    return a == b;
}
console.log(is('string', 'otherstring'));
console.log(is(true, false));
console.log(is(42, 42));
//# sourceMappingURL=04.js.map