"use strict";
const a = { value: 'a' };
const b = { value: 'b', isLeaf: true };
const c = { value: 'c', children: [b] };
const mapNode = (node, closure) => (Object.assign(Object.assign({}, node), { value: closure(node.value) }));
const a1 = mapNode(a, _ => _.toUpperCase());
const b1 = mapNode(b, _ => _.toUpperCase());
const c1 = mapNode(c, _ => _.toUpperCase());
console.log(a1, b1, c1);
//# sourceMappingURL=tree.js.map