"use strict";
const Sum = (a, b) => a + b;
const Min = (a, b) => a < b ? a : b;
const IsEven = (a) => a === 0 ? true : a === 1 ? false : a > 0 ? IsEven(a - 2) : IsEven(a * -1);
const CountB = (s) => {
    return CountChar(s, 'B');
};
const CountChar = (s, c) => {
    let count = 0;
    for (let i = 0; i < s.length; i++) {
        if (s[i] === c[0])
            count++;
    }
    return count;
};
console.log(Sum(4, 4));
console.log(Min(10, 4));
console.log(IsEven(50));
console.log(IsEven(75));
console.log(IsEven(-1));
console.log(CountB("BaBaba"));
console.log(CountChar("BaBaba", 'a'));
//# sourceMappingURL=chapter01-eloquent.js.map