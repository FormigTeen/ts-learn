"use strict";
const testX = {
    x: 1, y: 0, z: 0
};
function isBaseNormal(base) {
    return isVectorNormal(base.x) && isVectorNormal(base.y) && isVectorNormal(base.z);
}
//# sourceMappingURL=index.js.map