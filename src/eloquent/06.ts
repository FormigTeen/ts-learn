type GroupItemIterator<A> = {
    done: boolean
    value?: A
}

class Vec {
    constructor(
        public x: number,
        public y: number
    ) {
    }

    get length() : number {
        return Math.sqrt(this.x * this.x + this.y * this.y)
    }
    
    plus(vecA: Vec) : Vec {
        return new Vec(this.x + vecA.x, this.y + vecA.y)
    }

    minus(vecA: Vec) : Vec {
        return new Vec(this.x - vecA.x, this.y - vecA.y)
    }
}

class Group<A = unknown> {
    constructor(
        private items: A[] = []
    ){
    }

    has(item: A): boolean {
        return this.items.indexOf(item) >= 0;
    }

    add(item: A): boolean {
        if ( this.has(item) )
            return false;
        this.items.push(item)
        return true;
    }

    delete(item: A): boolean {
        if ( !this.has(item) )
            return false;
        this.items.splice(this.items.indexOf(item), 1)
        return true;
    }

    get(index: number) : A {
        return this.items[index];
    }

    get length() : number {
        return this.items.length
    }


}

class GroupIterator<A = unknown> {

    private index : number  = 0;

    constructor(
        private group: Group<A>
    ) {
    }

    next() : GroupItemIterator<A> {
        if ( this.index === this.group.length ) {
            return {
                done: true
            }
        } 

        const value = this.group.get(this.index)
        this.index = this.index + 1;
        return {
            done: false,
            value
        }
    }
}
