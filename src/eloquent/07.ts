type Road = string

type Place = string

type Parcel = {
    place: Place,
    address: Place,
}

type Action = {
    direction: Place,
    memory?: Place[],
}

type Graph = {
    [key: string]: Road[]
}

type Route = {
    at: Place,
    route: Place[],
}

type Robot = (state: VillageState, memory?: Place[]) => Action

const roads : Road[] = [
    "Alice's House-Bob's House",
    "Alice's House-Post Office",
    "Alice's House-Cabin",
    "Daria's House-Emie's House",
    "Daria's House-Town Hall",
    "Emie's House-Grete's House",
    "Grete's House-Shop",
    "Grete's House-Farm",
    "Marketplace-Post Office",
    "Marketplace-Town Hall",
    "Marketplace-Farm",
    "Marketplace-Shop",
    "Shop-Town Hall",
]

function buildGraph(roads: Road[]) : Graph {

    let graph = Object.create(null);

    function addEdge(from: Place, to: Place) {
        if ( graph[from] == null ) {
            graph[from] = [to];
        } else {
            graph[from].push(to);
        }
    }

    for ( let [from, to] of roads.map(_ => _.split('-')) ) {
        addEdge(from, to);
        addEdge(to, from);
    }

    return Object.freeze(graph);
}

const roadGraph = buildGraph(roads);

class VillageState {

    constructor(
        public place: Place, 
        public parcels: Parcel[]
    ) {}

    move(destination: Place) : VillageState {
        if (!roadGraph[this.place].includes(destination)) {
            return this;
        } else {
            let parcels = this.parcels.map(parcel => {
                if (parcel.place != this.place) return parcel;
                return {
                    place: destination,
                    address: parcel.address,
                }
            }).filter(parcel => parcel.place != parcel.address);
            return new VillageState(destination, parcels);
        }
    }

    static random(parcelCount : number = 5) : VillageState {
        let parcels : Parcel[]  = [];
        for ( let i = 0 ; i < parcelCount ; i++ ) {
            let address = randomPick(Object.keys(roadGraph));
            let place : Place;
            do {
                place = randomPick(Object.keys(roadGraph));
            } while (place === address);
            parcels.push({ place, address })
        }

        console.log({ parcels });
        return new VillageState("Post Office", parcels);
    }

}

function runRobot(state : VillageState, robot : Robot, memory? : Place[]) : void {
    for ( let turn = 0 ;; turn++ ) {
        if ( state.parcels.length == 0 ) {
            console.log(`Done in ${turn} turns`);
            break;
        }
        let action = robot(state, memory);
        state = state.move(action.direction);
        memory = action.memory;
        console.log(`Moved to ${action.direction}`);
    }
}

function randomPick<I>(array: I[]) : I {
    let choice = Math.floor(Math.random() * array.length);
    return array[choice];
}

function randomRobot(state: VillageState) : Action {
    return {
        direction: randomPick(roadGraph[state.place]),
    }
}

const mailRoute: Place[] = [
    "Alice's House",
    "Cabin",
    "Alice's House",
    "Bob's House",

    "Town Hall",
    "Daria's House",
    "Emie's House",

    "Grete's House",
    "Shop",
    "Grete's House",
    "Farm",

    "Marketplace",
    "Post Office",
];

function routeRobot(state: VillageState, memory: Place[] = []) : Action {
    if ( memory.length == 0 )
        memory = mailRoute;

    return {
        direction: memory[0],
        memory: memory.slice(1)
    }
}

function findRoute(graph: Graph, from: Place, to: Place): Place[] {
    let work: Route[] = [
        {at: from, route: []},
    ]
    for ( let i = 0 ; i < work.length ; i++ ) {
        let {at, route} = work[i];
        for ( let place of graph[at]) {
            if ( place == to ) return route.concat(place);
            if ( !work.some(w => w.at == place) ) {
                work.push({at: place, route: route.concat(place)})
            }
        }
    }
    return [];
}

function goalOrientedRobot(state: VillageState, memory: Place[] = []) : Action {
    if ( memory.length == 0 ) {
        let parcel = state.parcels[0];
        if ( parcel.place != state.place ) {
            memory = findRoute(roadGraph, state.place, parcel.place);
        } else {
            memory = findRoute(roadGraph, state.place, parcel.address);
        }
    }

    return {
        direction: memory[0],
        memory: memory.slice(1)
    }
}

runRobot(VillageState.random(5), goalOrientedRobot);
