function range(start: number, end: number): number[] {
    const totalElements = (end - start) + 1
    return [...Array(totalElements).keys()].map(index => index + start);
}

function sum(args: number[]) : number {
    return args.reduce((total, number) => total + number, 0)
}

function reverse<T extends unknown>(elements: T[]) : T[] {
    const result = [];
    for ( let i = elements.length - 1 ; i >= 0 ; i-- ) {
        const index = elements.length - i - 1;
       result[index] = elements[i];
    }
    return result
}

function reverseInPlace<T extends unknown>(elements: T[]) : T[] {
    const copy = [...elements];

    for ( let i = elements.length - 1 ; i >= 0 ; i-- ) {
       const index = elements.length - i - 1;
       elements[index] = copy[i];

    }
    return elements
}

console.log(reverse([1, 2, 3]));
console.log(reverseInPlace([1, 2, 3]));
console.log(sum(range(1, 100)))
