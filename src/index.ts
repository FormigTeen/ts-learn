interface Base {
    x: VectorBase,
    y: VectorBase,
    z: VectorBase,
}

interface VectorBase {
    x: number,
    y: number,
    z: number,
}

type VectorNormal<A extends keyof VectorBase> = {
    readonly [K in keyof VectorBase]: K extends A ? 1 : 0
}

type VectorNull = {
    readonly [K in keyof VectorBase]: 0
}

type BaseNormal = {
    [K in keyof Base]: VectorNormal<K>
}

const testX : VectorNormal<'x'> = {
    x: 1, y: 0, z: 0
}

interface Vector extends VectorBase {
    base: Base
}

function isBaseNormal(base: Base) : base is BaseNormal {
    return isVectorNormal(base.x) && isVectorNormal(base.y) && isVectorNormal(base.z);
}

