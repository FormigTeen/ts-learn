type TreeNode = {
    value: string
};

type LeafNode = TreeNode & {
    isLeaf: true
};

type InnerNode = TreeNode & {
    children: [TreeNode] | [TreeNode, TreeNode]
};

type mapTreeFunction = <U extends TreeNode>(n: U, c: (v: string) => string) => U;

const mapTreeArrow: mapTreeFunction = (node, closure) => ({
    ...node,
    value: closure(node.value)
});

function mapTree <U extends TreeNode>(node: U, closure: (v: string) => string) : U {
    return {
        ...node,
        value: closure(node.value)
    }
}

const a: TreeNode = { value: 'a' };
const b: LeafNode = { value: 'b', isLeaf: true };
const c: InnerNode = { value: 'c', children: [b] };

const a1 = mapTree(a, _ => _.toUpperCase());
const b1 = mapTree(b, _ => _.toUpperCase());
const c1 = mapTree(c, _ => _.toUpperCase());

type HasSides = {
    numberOfSides: number
}

type SidesHaveLength = {
    sideLength: number
}

type Square = HasSides & SidesHaveLength

function logPerimeter<Shape extends HasSides & SidesHaveLength>(s: Shape): Shape {
    console.log(s.numberOfSides * s.sideLength)
    return s
}

const square: Square = {
    numberOfSides: 4,
    sideLength: 3
}

logPerimeter(square)

function call<T extends unknown[], R>( f: (...args: T) => R, ...args: T ): R {
    return f(...args)
}

function fill(length: number, value: string) : string[] {
    return Array.from({length}, () => value);
}

function is<T extends unknown>(...args: T[]): boolean {
    return a == b
}

console.log(is('string', 'otherstring'));
console.log(is(true, false));
console.log(is(42, 42));

